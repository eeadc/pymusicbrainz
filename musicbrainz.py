# coding: utf-8

import psycopg2
import psycopg2.extras

class Musicbrainz(object):
	models = []
	fc_models = []

	def __init__(self, connection):
		self.connection = connection

	def cursor(self):
		return self.connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

	def find_by_mbid(self, mbid):
		for model in self.fc_models:
			record = model.find_by_mbid(self, mbid)
			if record is not None:
				return record
		return None

	def find_artist_by_id(self, id):
		return Artist.find_by_id(self, id)

	def find_artist_by_mbid(self, mbid):
		return Artist.find_by_mbid(self, mbid)

	def find_artist_credit_by_id(self, id):
		return ArtistCredit.find_by_id(self, id)

	def find_artist_credit_name_by_id(self, id):
		return ArtistCreditName.find_by_id(self, id)

	def find_artist_name_by_id(self, id):
		return ArtistName.find_by_id(self, id)

	def find_artist_type_by_id(self, id):
		return ArtistType.find_by_id(self, id)

	def find_country_by_id(self, id):
		return Country.find_by_id(self, id)

	def find_gender_by_id(self, id):
		return Gender.find_by_id(self, id)

	def find_language_by_id(self, id):
		return Language.find_by_id(self, id)

	def find_recording_by_id(self, id):
		return Recording.find_by_id(self, id)

	def find_recording_by_mbid(self, mbid):
		return Recording.find_by_mbid(self, mbid)

	def find_release_by_id(self, id):
		return Release.find_by_id(self, id)

	def find_release_by_mbid(self, mbid):
		return Release.find_by_mbid(self, mbid)

	def find_release_group_by_id(self, id):
		return ReleaseGroup.find_by_id(self, id)

	def find_release_group_by_mbid(self, mbid):
		return ReleaseGroup.find_by_mbid(self, mbid)
	
	def find_release_group_primary_type_by_id(self, id):
		return ReleaseGroupPrimaryType.find_by_id(self, id)

	def find_release_name_by_id(self, id):
		return ReleaseName.find_by_id(self, id)

	def find_release_packaging_by_id(self, id):
		return ReleasePackaging.find_by_id(self, id)

	def find_release_status_by_id(self, id):
		return ReleaseStatus.find_by_id(self, id)

	def find_script_by_id(self, id):
		return Script.find_by_id(self, id)

	def find_track_by_id(self, id):
		return Track.find_by_id(self, id)

	def find_tracklist_by_id(self, id):
		return Tracklist.find_by_id(self, id)

	def find_track_name_by_id(self, id):
		return TrackName.find_by_id(self, id)

class Record(object):
	_id = None
	_record = None
	_mb = None

	def __init__(self, record, mb):
		self._mb = mb
		self._record = record
		self._id = record.get("id", None)

	def __repr__(self):
		return "<{cls} id={id} mbid={mbid}>".format(
			cls=type(self).__name__,
			id=self._id,
			mbid=getattr(self, "mbid", None)
		)

	def __eq__(self, other):
		return (type(self) == type(other)) and (self._id == other._id)
	
	def __ne__(self, other):
		return not self == other

	@classmethod
	def find_by_id(cls, mb, id):
		return cls.find_one(
			mb,
			"SELECT * FROM {0} WHERE id = %s LIMIT 1;".format(cls.__table__),
			(id,)
		)

	@classmethod
	def find_by_mbid(cls, mb, mbid):
		if not hasattr(cls, "mbid"):
			raise RuntimeError("{0} doesn't supports MBID".format(cls.__name__))

		return cls.find_one(
			mb,
			"SELECT * FROM {0} WHERE gid = %s LIMIT 1;".format(cls.__table__),
			(mbid,)
		)

	@classmethod
	def find_many(cls, mb, query, params=()):
		cur = mb.cursor()
		cur.execute(query, params)
		records = cur.fetchall()
		cur.close()
		return [cls(r, mb) for r in records]

	@classmethod
	def find_one(cls, mb, query, params=()):
		cur = mb.cursor()
		cur.execute(query, params)
		if cur.rowcount == 0:
			return None
		record = cur.fetchone()
		cur.close()
		return cls(record, mb)

	@classmethod
	def find(cls, mb, query, params=()):
		cur = mb.cursor()
		cur.execute(query, params)
		
		return (cls(record, mb) for record in cur), cur

	@classmethod
	def all(cls, mb):
		return cls.find(
			mb,
			"SELECT * FROM {};".format(cls.__table__)
		)

class Country(Record):
	__table__ = "country"

	iso_code = None
	name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.iso_code = record["iso_code"]
		self.name = record["name"]

Musicbrainz.models.append(Country)

class ArtistType(Record):
	__table__ = "artist_type"

	name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.name = record["name"]
	
	def __str__(self):
		return self.name

Musicbrainz.models.append(ArtistType)

class ArtistName(Record):
	__table__ = "artist_name"

	name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.name = record["name"]
	
	def __str__(self):
		return self.name

	@property
	def artists(self):
		return Artist.find_many(
			self._mb,
			"SELECT * FROM artist WHERE name = %s;",
			(self._id,)
		)

	@property
	def artist(self):
		return self.artists[0]

	@classmethod
	def find_by_name(cls, mb, name):
		return cls.find_one(
			mb,
			"SELECT * FROM artist_name WHERE name = %s;",
			(name,)
		)

Musicbrainz.models.append(ArtistName)

class Gender(Record):
	__table__ = "gender"

	name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.name = record["name"]
	
	def __str__(self):
		return self.name

Musicbrainz.models.append(Gender)

class Artist(Record):
	__table__ = "artist"

	mbid = None
	name_id = None
	sort_name_id = None
	type_id = None
	country_id = None
	gender_id = None

	_name = None
	_sort_name = None
	_artist_type = None
	_country = None
	_gender = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.mbid = record["gid"]
		self.name_id = record["name"]
		self.sort_name_id = record["sort_name"]
		self.type_id = record["type"]
		self.country_id = record["country"]
		self.gender_id = record["gender"]
	
	@property
	def name(self):
		if self._name is None:
			self._name = ArtistName.find_by_id(self._mb, self.name_id)
		return self._name

	@property
	def sort_name(self):
		if self._sort_name is None:
			self._sort_name = ArtistName.find_by_id(self._mb, self.name_id)
		return self._sort_name

	@property
	def artist_type(self):
		if self._artist_type is None:
			self._artist_type = ArtistType.find_by_id(self._mb, self.artist_type_id)
		return self._artist_name

	@property
	def country(self):
		if self._country is None:
			self._country = Country.find_by_id(self._mb, self.country_id)
		return self._country

	@property
	def gender(self):
		if self._gender is None:
			self._gender = Gender.find_by_id(self._mb, self.gender_id)
		return self._gender

Musicbrainz.models.append(Artist)
Musicbrainz.fc_models.append(Artist)

class Recording(Record):
	__table__ = "recording"

	mbid = None
	artist_credit_id = None
	name_id = None
	length = None
	comment = None

	_artist_credit = None
	_name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.mbid = record["gid"]
		self.artist_credit_id = record["artist_credit"]
		self.name_id = record["name"]
		self.length = record["length"]
		self.comment = record["comment"]

	@property
	def artist_credit(self):
		if self._artist_credit is None:
			self._artist_credit = ArtistCredit.find_by_id(self._mb, self.artist_credit_id)
		return self._artist_credit

	@property
	def name(self):
		if self._name is None:
			self._name = TrackName.find_by_id(self._mb, self.name_id)
		return self._name

	@property
	def artists(self):
		return [artist_credit_name.artist for artist_credit_name in self.artist_credit.artist_credit.names]

	@property
	def artist(self):
		return self.artists[0]

Musicbrainz.models.append(Recording)
Musicbrainz.fc_models.append(Recording)

class TrackName(Record):
	__table__ = "track_name"

	name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.name = record["name"]

	@property
	def tracks(self):
		return Track.find_many(
			self._mb,
			"SELECT * FROM track WHERE name = %s;",
			(self._id,)
		)

	@property
	def artist(self):
		return self.tracks[0].recording.artist_credit.artist_credit_names[0].artist

	@classmethod
	def find_by_name(cls, mb, name):
		return cls.find_one(
			mb,
			"SELECT * FROM track_name WHERE name = %s;",
			(name,)
		)

Musicbrainz.models.append(TrackName)

class ArtistCredit(Record):
	__table__ = "artist_credit"

	name_id = None
	artist_count = None
	ref_count = None
	created = None

	_name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.name_id = record["name"]
		self.artist_count = record["artist_count"]
		self.ref_count = record["ref_count"]
		self.created = record["created"]

	@property
	def artist_credit_names(self):
		return ArtistCreditName.find_many(
			self._mb,
			"SELECT * FROM artist_credit_name WHERE artist_credit = %s;",
			(self._id,)
		)

	@property
	def name(self):
		if self._name is None:
			self._name = ArtistName.find_by_id(self._mb, self.name_id)
		return self._name

Musicbrainz.models.append(ArtistCredit)

class ArtistCreditName(Record):
	artist_credit_id = None
	artist_id = None
	name_id = None
	position = None

	_artist_credit = None
	_artist = None
	_name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.artist_credit_id = record["artist_credit"]
		self.artist_id = record["artist"]
		self.name_id = record["name"]
		self.position = record["position"]

	@property
	def artist_credit(self):
		if self._artist_credit is None:
			self._artist_credit = ArtistCredit.find_by_id(self._mb, self.artist_credit_id)
		return self._artist_credit

	@property
	def artist(self):
		if self._artist is None:
			self._artist = Artist.find_by_id(self._mb, self.artist_id)
		return self._artist
	
	@property
	def name(self):
		if self._name is None:
			self._name = ArtistName.find_by_id(self._mb, self.name_id)
		return self._name

	def __repr__(self):
		return "<ArtistCreditName artist_credit={artist_credit} position={position}>".format(
			artist_credit=self.artist_credit,
			position=self.position
		)

	@classmethod
	def find_by_artist_credit(cls, mb, artist_credit):
		return cls.find_many(
			mb,
			"SELECT * FROM artist_credit_name WHERE artist_credit = %s;",
			(artist_credit,)
		)

	@classmethod
	def find_by_artist_credit_and_position(cls, mb, artist_credit, position):
		return cls.find_one(
			mb,
			"SELECT * FROM artist_credit_name WHERE artist_credit = %s AND position = %s;",
			(artist_credit, position)
		)

Musicbrainz.models.append(ArtistCreditName)

class Track(Record):
	__table__ = "track"

	recording_id = None
	tracklist_id = None
	position = None
	number = None
	name_id = None
	artist_credit_id = None
	length = None

	_recording = None
	_tracklist = None
	_name = None
	_artist_credit = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.recording_id = record["recording"]
		self.tracklist_id = record["tracklist"]
		self.position = record["position"]
		self.number = record["number"]
		self.name_id = record["name"]
		self.artist_credit_id = record["artist_credit"]
		self.length = record["length"]
	
	@property
	def recording(self):
		if self._recording is None:
			self._recording = Recording.find_by_id(self._mb, self.recording_id)
		return self._recording

	@property
	def tracklist(self):
		if self._tracklist is None:
			self._tracklist = Tracklist.find_by_id(self._mb, self.tracklist_id)
		return self._tracklist

	@property
	def name(self):
		if self._name is None:
			self._name = TrackName.find_by_id(self._mb, self.name_id)
		return self._name

	@property
	def artist_credit(self):
		if self._artist_credit is None:
			self._artist_credit = ArtistCredit.find_by_id(self._mb, self.artist_credit_id)
		return self._artist_credit

	@property
	def artists(self):
		return [artist_credit_name.artist for artist_credit_name in self.artist_credit.artist_credit_names]

	@property
	def artist(self):
		return self.artists[0]

Musicbrainz.models.append(Track)

class Tracklist(Record):
	__table__ = "tracklist"

	track_count = None
	last_updated = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.track_count = record["track_count"]
		self.last_updated = record["last_updated"]
	
	@property
	def tracks(self):
		return Track.find_many(
			self._mb,
			"SELECT * FROM track WHERE tracklist = %s ORDER BY position ASC;",
			(self._id,)
		)

Musicbrainz.models.append(Tracklist)

class Release(Record):
	__table__ = "release"

	mbid = None
	release_group_id = None
	artist_credit_id = None
	name_id = None
	barcode = None
	country_id = None
	status_id = None
	packaging_id = None
	language_id = None
	script_id = None
	comment = None
	quality = None
	date_year = None
	date_month = None
	date_day = None

	_release_group = None
	_artist_credit = None
	_name = None
	_country = None
	_status = None
	_packaging = None
	_language = None
	_script = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.mbid = record["gid"]
		self.release_group_id = record["release_group"]
		self.artist_credit_id = record["artist_credit"]
		self.name_id = record["name"]
		self.barcode = record["barcode"]
		self.country_id = record["country"]
		self.status_id = record["status"]
		self.packaging_id = record["packaging"]
		self.language_id = record["language"]
		self.script_id = record["script"]
		self.comment = record["comment"]
		self.quality = record["quality"]
		self.date_year = record["date_year"]
		self.date_month = record["date_month"]
		self.date_day = record["date_day"]

	@property
	def release_group(self):
		if self._release_group is None:
			self._release_group = ReleaseGroup.find_by_id(self._mb, self.release_group_id)
		return self._release_group

	@property
	def artist_credit(self):
		if self._artist_credit is None:
			self._artist_credit = ArtistCredit.find_by_id(self._mb, self.artist_credit_id)
		return self._artist_credit
	
	@property
	def name(self):
		if self._name is None:
			self._name = ReleaseName.find_by_id(self._mb, self.name_id)
		return self._name

	@property
	def country(self):
		if self._country is None:
			self._country = Country.find_by_id(self._mb, self.country_id)
		return self._country

	@property
	def status(self):
		if self._status is None:
			self._status = ReleaseStatus.find_by_id(self._mb, self.status_id)
		return self._status

	@property
	def packaging(self):
		if self._packaging is None:
			self._packaging = ReleasePackaging.find_by_id(self._mb, self.packaging_id)
		return self._packaging

	@property
	def language(self):
		if self._language is None:
			self._language = Language.find_by_id(self._mb, self.language_id)
		return self._language

	@property
	def script(self):
		if self._script is None:
			self._script = Script.find_by_id(self._mb, self.script_id)
		return self._script

	@property
	def mediums(self):
		return Medium.find_many(
			self._mb,
			"SELECT * FROM medium WHERE release = %s ORDER BY position ASC;",
			(self._id,)
		)

	@property
	def artists(self):
		return [artist_credit_name.artist for artist_credit_name in self.artist_credit.artist_credit_names]

	@property
	def artist(self):
		return self.artists[0]

Musicbrainz.models.append(Release)
Musicbrainz.fc_models.append(Release)

class ReleaseGroup(Record):
	__table__ = "release_group"

	mbid = None
	name_id = None
	artist_credit_id = None
	type_id = None
	comment = None
	
	_name = None
	_artist_credit = None
	_type = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.mbid = record["gid"]
		self.name_id = record["name"]
		self.artist_credit_id = record["artist_credit"]
		self.type_id = record["type"]
		self.comment = record["comment"]

	@property
	def name(self):
		if self._name is None:
			self._name = ReleaseName.find_by_id(self._mb, self.name_id)
		return self._name
	
	@property
	def artist_credit(self):
		if self._artist_credit is None:
			self._artist_credit = ArtistCredit.find_by_id(self._mb, self.artist_credit_id)
		return self._artist_credit
	
	@property
	def type(self):
		if self._type is None:
			self._type = ReleaseGroupPrimaryType.find_by_id(self._mb, self.type_id)
		return self._type

	@property
	def artists(self):
		return [artist_credit_name.artist for artist_credit_name in self.artist_credit.artist_credit_names]

	@property
	def artist(self):
		return self.artists[0]

Musicbrainz.models.append(ReleaseGroup)
Musicbrainz.fc_models.append(ReleaseGroup)

class ReleaseName(Record):
	__table__ = "release_name"

	name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.name = record["name"]

Musicbrainz.models.append(ReleaseName)

class Language(Record):
	__table__ = "language"

	iso_code_3 = None
	iso_code_2t = None
	iso_code_2b = None
	iso_code_1 = None
	name = None
	frequency = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.iso_code_3 = record["iso_code_3"]
		self.iso_code_2t = record["iso_code_2t"]
		self.iso_code_2b = record["iso_code_2b"]
		self.iso_code_1 = record["iso_code_1"]
		self.name = record["name"]
		self.frequency = record["frequency"]

Musicbrainz.models.append(Language)

class ReleaseGroupPrimaryType(Record):
	__table__ = "release_group_primary_type"

	name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.name = record["name"]

Musicbrainz.models.append(ReleaseGroupPrimaryType)

class ReleasePackaging(Record):
	__table__ = "release_packaging"

	name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.name = record["name"]

Musicbrainz.models.append(ReleasePackaging)

class ReleaseStatus(Record):
	__table__ = "release_status"

	name = None
	
	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.name = record["name"]

Musicbrainz.models.append(ReleaseStatus)

class Script(Record):
	__table__ = "script"

	iso_code = None
	iso_number = None
	name = None
	frequency = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.iso_code = record["iso_code"]
		self.iso_number = record["iso_number"]
		self.name = record["name"]
		self.frequency = record["frequency"]

Musicbrainz.models.append(Script)

class Link(Record):
	__table__ = "link"

	link_type_id = None

	_link_type = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.link_type_id = record["link_type"]
	
	@property
	def link_type(self):
		if self._link_type is None:
			self._link_type = LinkType.find_by_id(self._mb, self.link_type_id)
		return self._link_type

Musicbrainz.models.append(Link)

class LinkType(Record):
	__table__ = "link_type"

	mbid = None
	parent_id = None
	entity_type0 = None
	entity_type1 = None
	name = None
	description = None
	link_phrase = None
	reverse_link_phrase = None
	short_link_phrase = None
	priority = None

	_parent = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.parent_id = record["parent"]
	
	@property
	def parent(self):
		if self._parent is None:
			self._parent = LinkType.find_by_id(self._mb, self.parent_id)
		return self._link_type

Musicbrainz.models.append(LinkType)
Musicbrainz.fc_models.append(LinkType)

class Medium(Record):
	__table__ = "medium"
	
	tracklist_id = None
	release_id = None
	position = None
	format_id = None
	name = None

	_tracklist = None
	_release = None
	_format = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.tracklist_id = record["tracklist"]
		self.release_id = record["release"]
		self.position = record["position"]
		self.format_id = record["format"]
		self.name = record["name"]
	
	@property
	def release(self):
		if self._release is None:
			self._release = Release.find_by_id(self._mb, self.release_id)
		return self._release

	@property
	def tracklist(self):
		if self._tracklist is None:
			self._tracklist = Tracklist.find_by_id(self._mb, self.tracklist_id)
		return self._tracklist
	
	@property
	def format(self):
		if self._format is None:
			self._format = MediumFormat.find_by_id(self._mb, self.format_id)
		return self._format

Musicbrainz.models.append(Medium)

class MediumFormat(Record):
	__table__ = "medium_format"

	name = None
	year = None
	parent_id = None
	child_order = None

	_parent = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.name = record["name"]
		self.year = record["year"]
		self.parent_id = record["parent"]
		self.child_order = record["child_order"]
	
	@property
	def parent(self):
		if self._parent is None:
			self._parent = MediumFormat.find_by_id(self._mb, self.parent_id)
		return self._parent

	@property
	def origin(self):
		origin = self
		while self.parent is not None and self.parent != origin:
			origin = self.parent
		return origin

Musicbrainz.models.append(Medium)

class Tag(Record):
	__table__ = "tag"

	name = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.name = record["name"]

	@property
	def recording_tags(self):
		return RecordingTag.find_many(
			self._mb,
			"SELECT * FROM recording_tag WHERE tag = %s;",
			(self._id,)
		)

	@property
	def artist_tags(self):
		return ArtistTag.find_many(
			self._mb,
			"SELECT * FROM artist_tag WHERE tag = %s;",
			(self._id,)
		)
	
	@property
	def release_tags(self):
		return ReleaseTag.find_many(
			self._mb,
			"SELECT * FROM release_tag WHERE tag = %s;",
			(self._id,)
		)

	@property
	def releases(self):
		return [release_tag.release for release_tag in self.release_tags]

	@property
	def artists(self):
		return [artist_tag.artist for artist_tag in self.artist_tags]

	@property
	def recordings(self):
		return [recording_tag.recording for recording_tag in self.recording_tags]

Musicbrainz.models.append(Tag)

class RecordingTag(Record):
	__table__ = "recording_tag"

	count = None
	recording_id = None
	tag_id = None

	_recording = None
	_tag = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.count = record["count"]
		self.recording_id = record["recording"]
		self.tag_id = record["tag"]

	@property
	def recording(self):
		if self._recording is None:
			self._recording = Recording.find_by_id(self._mb, self.recording_id)
		return self._recording
	
	@property
	def tag(self):
		if self._tag is None:
			self._tag = Tag.find_by_id(self._mb, self.tag_id)
		return self._tag

Musicbrainz.models.append(RecordingTag)

class ArtistTag(Record):
	__table__ = "artist_tag"

	count = None
	artist_id = None
	tag_id = None

	_artist = None
	_tag = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.count = record["count"]
		self.artist_id = record["artist"]
		self.tag_id = record["tag"]

	@property
	def artist(self):
		if self._artist is None:
			self._artist = Artist.find_by_id(self._mb, self.artist_id)
		return self._artist

	@property
	def tag(self):
		if self._tag is None:
			self._tag = Tag.find_by_id(self._mb, self.tag_id)
		return self._tag

Musicbrainz.models.append(ArtistTag)

class ReleaseTag(Record):
	__table__ = "release_tag"

	count = None
	release_id = None
	tag_id = None

	_release = None
	_tag = None

	def __init__(self, record, mb):
		Record.__init__(self, record, mb)

		self.count = record["count"]
		self.release_id = record["release"]
		self.tag_id = record["tag"]

	@property
	def release(self):
		if self._release is None:
			self._release = Release.find_by_id(self._mb, self.release_id)
		return self._release

	@property
	def tag(self):
		if self._tag is None:
			self._tag = Tag.find_by_id(self._mb, self.tag_id)
		return self._tag

Musicbrainz.models.append(ReleaseTag)

